<?php

namespace App\Http\Controllers\User\Product;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class ProdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        return view('user.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('user.product.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name'=>['required', 'string', 'min:3', 'unique:products'],
            'description'=>['required', 'min:5'],
            'category_id' => ['required', 'integer'],
            'price' => ['required', 'integer'],
            'expire_at' => ['required', 'after'],
        ]);

        $contact = new Product([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'category_id' => $request->get('category_id'),
            'price' => $request->get('price'),
            'expire_at' => $request->get('expire_at')
        ]);
        $contact->save();
        return redirect()->route('product.index')->with('success', 'product saved!');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);

        return view('product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();

        return view('user.product.edit', compact('product', 'categories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>['required', 'string', 'min:3', Rule::unique('products')->ignore($id)],
            'description'=>['required', 'min:5'],
            'category_id' => ['required', 'integer', ],
            'price' => ['required', 'integer'],
            'expire_at' => ['required', 'after'],
        ]);

        $product = Product::find($id);
        $product->name = $request->get('name');
        $product->description = $request->get('description');
        $product->category_id = $request->get('category_id');
        $product->price = $request->get('price');
        $product->expire_at = $request->get('expire_at');
        $product->save();

        return redirect()->route('product.index')->with('success', 'Product updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Product::find($id);
        $contact->delete();

        return redirect()->route('product.index')->with('success', 'Product deleted!');
    }

}
