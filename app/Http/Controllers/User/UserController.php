<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    // users controller
    public function login(){
        return \view('user.auth.login');
    }

    public function register(){
        return \view('user.auth.register');
    }

    public function index(){
        return view('user.auth.home');
    }


}
