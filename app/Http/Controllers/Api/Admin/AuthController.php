<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthController extends JsonResource
{
    public function register(){
        $data = [
            'name' => $this->name,
            'email' => $this->email,
            'username' => $this->username,
            'password' => $this->password,
            'confirm_password' => $this->confirm_password,
        ];

        return $this->response()->json($data);
    }


    public function login(){
        $data = [
            'email' => $this->email,
            'username' => $this->username,
            'password' => $this->password,
        ];

        return $this->response()->json($data);
    }

}
