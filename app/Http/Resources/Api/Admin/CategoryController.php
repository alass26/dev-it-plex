<?php

namespace App\Http\Resources\Api\Admin;

use App\Models\Category;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryController extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    /**
     * Return a listing of the resource.
     *
     * @return Illuminate\Http\Resources\Json\JsonResource
     */
    public function index()
    {
        $data = Category::all();

        return response()->json($data);
    }

    public function show($id)
    {
        $data = Category::find($id);

        return response()->json($data);
    }

    public function create($request)
    {
        $data = [
            "name"=>$request->name,
            "description"=>$request->description,
        ];

        return response()->json($data);
    }

    public function update($id)
    {
        $data = [

        ];
        return response()->json($data);
    }

    public function delete($id)
    {
        return destroy(Category::find($id));
    }

}
