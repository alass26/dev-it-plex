<?php

namespace App\Http\Resources\Api\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class AuthController extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function register(){
        $data = [
            'name' => $this->name,
            'email' => $this->email,
            'username' => $this->username,
            'password' => $this->password,
            'confirm_password' => $this->confirm_password,
        ];

        return $this->response()->json($data);
    }


    public function login(){
        $data = [
            'email' => $this->email,
            'username' => $this->username,
            'password' => $this->password,
        ];

        return $this->response()->json($data);
    }

}
