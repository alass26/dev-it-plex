<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */


    protected function redirectTo($request)
    {

        if (!$request->expectsJson()) {
            return 'user_login_page';
        }
        /**
        if (!$request->expectsJson()) {
            if ($request->is('/user/product/*') or $request->is('/user/product')) {

                    return route('user_login_page');
            }

            if ($request->is('/admin/category/*') or $request->is('/user/category')){

                    return route('admin_login_page');

            }

        }
         */
    }
}
