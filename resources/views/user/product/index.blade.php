@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h3 class="display-3">Products</h3>

            <a href="{{ route('product.create')}}" class="btn btn-success btn-lg">Create</a>
            <a href="{{ route('user.home')}}" class="btn btn-success btn-lg">Home</a>

            @if(count($products) > 0)
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Description</td>
                        <td>category</td>
                        <td>price</td>
                        <td>expire_at</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)

                        <tr>
                            <td>{{$product->id}}</td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->description}}</td>
                            <td>{{$product->category->name}}</td>
                            <td>{{$product->price}}</td>
                            <td>{{$product->expire_at}}</td>

                            <td>
                                <a href="{{ route('product.show',$product->id)}}" class="btn btn-primary">Show</a>
                            </td>
                            <td>
                                <a href="{{ route('product.edit',$product->id)}}" class="btn btn-primary">Edit</a>
                            </td>
                            <td>
                                <form action="{{ route('product.destroy', $product->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <h4 style="color:#01549b">Aucun événement pour le moment</h4>
            @endif
            <div>
            </div>
@endsection
