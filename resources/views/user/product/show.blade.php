@extends('layouts.base')

@section('content')
    <div align="center" class="row">
        <div class="col-sm-12">
            <h3 class="display-4"> Produit N°{{ $product->id }}</h3>

            <p><label class="field left">Name : </label> <input type="text" value="{{$product->name}}" class="field left" readonly></p>
            <p><label class="field left">Description : </label> <input type="text" value="{{$product->description}}" class="field left" readonly></p>
            <p><label class="field left">Category id : </label> <input type="text" value="{{$product->category_id}}" class="field left" readonly></p>
            <p><label>Price : </label> <input type="text" value="{{$product->price}}" class="field left" readonly></p>
            <p><label>Expire at : </label> <input type="text" value="{{$product->expire_at}}" class="field left" readonly></p>
        </div>
    </div>
@endsection
