@extends('layouts.userApp')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">You are logged as user</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        <p><a href="{{ route('product.index')}}" class="btn btn-success btn-lg">Lister les produits</a></p>
                        <p><a href="{{ route('product.create')}}" class="btn btn-success btn-lg">Créer un produit</a></p>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
