@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h3 class="display-4">Catégorie N°{{ $category->id }}</h3>
            <p><label class="field left">Name : </label>
                <input type="text" value="{{$category->name}}" class="field left" readonly></p>
            <p><label class="field left">Description : </label>
                <input type="text" value="{{$category->description}}" class="field left" readonly></p>
        </div>
    </div>
@endsection
