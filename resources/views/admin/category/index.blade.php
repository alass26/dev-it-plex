@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h3 class="display-3">Categories</h3>

            <a href="{{ route('category.create')}}" class="btn btn-success btn-lg">Create</a>
            <a href="{{ route('admin.home') }}" class="btn btn-success btn-lg">Home</a>

            @if(count($categories) > 0)
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Description</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{$category->id}}</td>
                            <td>{{$category->name}}</td>
                            <td>{{$category->description}}</td>

                            <td>
                                <a href="{{ route('category.show',$category->id)}}" class="btn btn-primary">Show</a>
                            </td>
                            <td>
                                <a href="{{ route('category.edit',$category->id)}}" class="btn btn-Primary">Edit</a>
                            </td>
                            <td>
                                <form action="{{ route('category.destroy', $category->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <h4 style="color:#01549b">Aucun événement pour le moment</h4>
            @endif
            <div>
            </div>
@endsection
