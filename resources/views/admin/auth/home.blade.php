@extends('layouts.adminApp')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">You are logged as admin</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p><a href="{{ route('category.index')}}" class="btn btn-success btn-lg">Lister les catégories</a></p>
                    <p><a href="{{ route('category.create')}}" class="btn btn-success btn-lg">Créer une catégorie</a></p>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
