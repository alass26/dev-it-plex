<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
/**
Route::resource('admin/category', 'Admin\Category\CateController')->middleware('auth:admin');
Route::resource('user/product', 'User\Product\ProdController')->middleware('auth:user');
*/

Route::resource('admin/category', 'Admin\Category\CateController');
Route::resource('user/product', 'User\Product\ProdController');

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::post('/', function () {
    return view('welcome');
});
// User uri

Route::get('user/login', 'User\Auth\LoginController@showLoginForm')->name('user_login_page');
Route::get('/user/register', 'User\Auth\RegisterController@showRegistrationForm')->name('user_register_page');

Route::get('/admin/login','Admin\Auth\LoginController@showLoginForm')->name('admin_login_page');
Route::get('/admin/register','Admin\Auth\RegisterController@showRegistrationForm')->name('admin_register_page');

Route::post('user/login', 'User\Auth\LoginController@login')->name('user.login');
Route::post('admin/login', 'Admin\Auth\LoginController@login')->name('admin.login');

Route::post('user/register','User\Auth\RegisterController@register')->name('user.register');
Route::post('admin/register','Admin\Auth\RegisterController@register')->name('admin.register');

Route::post('logout','User\Auth\LoginController@logout')->name('logout');

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user/home', 'User\UserController@index')->name('user.home');
Route::get('/admin/home', 'Admin\AdminController@index')->name('admin.home');
