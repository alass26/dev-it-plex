<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('admin/register', 'Api\Admin\AuthController@register');
Route::post('admin/login', 'Api\Admin\AuthController@login');

Route::get('admin/categories', 'Api\Admin\CategoryController@index');
Route::get('admin/categories/{id}/show', 'Api\Admin\CategoryController@show');
Route::post('admin/categories/create', 'Api\Admin\CategoryController@create');
Route::post('admin/categories/{id}/update', 'Api\Admin\CategoryController@update');
Route::delete('admin/categories/delete', 'Api\Admin\CategoryController@delete');

Route::get('test-get',  function (Request $request) {
    return response()->json(['server-ip'=> $request->ip()]);
});

Route::post('test-post',  function (Request $request) {
    return response()->json(['username'=>'alassani', 'password'=>'sourisBleu']);
});
